# Scriptable Object Workflow Package
Package that contains common used scripts to build an app using the ScriptableObjects.

## Requirements
* Unity 2020.3 or later.

## Installing the Package
* Add the name of the package (found in package.json) followed by the repository URL to your Unity project's manifest.json or using the package manager in Unity.

```json
{
  "dependencies": {
    "com.bourdonvalentin.scriptable-object-workflow": "https://gitlab.com/valentin_bourdon/ScriptableObject_Workflow.git",
}
```

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies), provided under the Apache License 2.0, see [COPY](./COPY) file.
All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
