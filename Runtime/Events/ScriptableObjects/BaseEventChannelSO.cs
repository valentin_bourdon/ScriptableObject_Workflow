using UnityEngine.Events;

namespace ScriptableObjectWorkflow.Events
{

	public abstract class BaseEventChannelSO<T> : DescriptionBaseSO
	{
		public UnityAction<T> OnEventRaised;
		public void RaiseEvent(T value)
		{
			if (OnEventRaised != null)
				OnEventRaised.Invoke(value);
		}
	}
}