using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{

    /// <summary>
    /// This class is used for Events that have a bool argument.
    /// Example: An event to toggle a UI interface
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Bool Event Channel", fileName = "NewBoolEvent_Channel")]
    public class BoolEventChannelSO : BaseEventChannelSO<bool>
    {
    }
}
