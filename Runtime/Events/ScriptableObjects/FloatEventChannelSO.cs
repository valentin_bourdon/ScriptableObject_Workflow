using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{

    /// <summary>
    /// This class is used for Events that have a float argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Float Event Channel", fileName = "NewFloatEvent_Channel")]
    public class FloatEventChannelSO : BaseEventChannelSO<float>
    {
    }
}
