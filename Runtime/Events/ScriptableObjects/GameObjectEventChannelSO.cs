using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{

    /// <summary>
    /// This class is used for Events that have a GameObject argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/GameObject Event Channel", fileName = "NewGameObjectEvent_Channel")]
    public class GameObjectEventChannelSO : BaseEventChannelSO<GameObject>
    {
    }
}
