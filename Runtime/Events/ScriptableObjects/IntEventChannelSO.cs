using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{
    /// <summary>
    /// This class is used for Events that have a int argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Int Event Channel", fileName = "NewIntEvent_Channel")]
    public class IntEventChannelSO : BaseEventChannelSO<int>
    {
    }
}
