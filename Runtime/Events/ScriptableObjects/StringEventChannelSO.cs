using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{

    /// <summary>
    /// This class is used for Events that have a string argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/String Event Channel", fileName = "NewStringEvent_Channel")]
    public class StringEventChannelSO : BaseEventChannelSO<string>
    {
    }
}