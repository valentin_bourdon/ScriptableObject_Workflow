using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{
    /// <summary>
    /// This class is used for Events that have a Transform argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Transform Event Channel", fileName = "NewTransformEvent_Channel")]
    public class TransformEventChannelSO : BaseEventChannelSO<Transform>
    {
    }
}
