using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{
    /// <summary>
    /// This class is used for Events that have a Vector2 argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Vector2 Event Channel", fileName = "NewVector2Event_Channel")]
    public class Vector2EventChannelSO : BaseEventChannelSO<Vector2>
    {
    }
}
