using UnityEngine;

namespace ScriptableObjectWorkflow.Events
{

    /// <summary>
    /// This class is used for Events that have a Vector3 argument.
    /// </summary>
    [CreateAssetMenu(menuName = "Events/Commun/Vector3 Event Channel", fileName = "NewVector3Event_Channel")]
    public class Vector3EventChannelSO : BaseEventChannelSO<Vector3>
    {
    }
}
