using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectWorkflow.Events
{
	/// <summary>
	/// This class is used for Events that have a no argument.
	/// </summary>
	[CreateAssetMenu(menuName = "Events/Commun/Void Event Channel", fileName = "NewVoidEvent_Channel")]
	public class VoidEventChannelSO : DescriptionBaseSO
	{
		public UnityAction OnEventRaised;

		public void RaiseEvent()
		{
			if (OnEventRaised != null)
				OnEventRaised.Invoke();
		}
	}
}