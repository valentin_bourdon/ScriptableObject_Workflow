using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectWorkflow.Events
{

	/// <summary>
	/// To use a generic UnityEvent type you must override the generic type.
	/// </summary>
	[System.Serializable]
	public class TransformEvent : UnityEvent<Transform>
	{

	}

	/// <summary>
	/// A flexible handler for bool events in the form of a MonoBehaviour. Responses can be connected directly from the Unity Inspector.
	/// </summary>
	public class TransformEventListener : MonoBehaviour
	{
		[SerializeField] private TransformEventChannelSO _channel = default;

		public TransformEvent OnEventRaised;

		private void OnEnable()
		{
			if (_channel != null)
				_channel.OnEventRaised += Respond;
		}

		private void OnDisable()
		{
			if (_channel != null)
				_channel.OnEventRaised -= Respond;
		}

		private void Respond(Transform value)
		{
			if (OnEventRaised != null)
				OnEventRaised.Invoke(value);
		}
	}
}