using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjectWorkflow.Events
{

	/// <summary>
	/// To use a generic UnityEvent type you must override the generic type.
	/// </summary>
	[System.Serializable]
	public class Vector3Event : UnityEvent<Vector3>
	{

	}

	/// <summary>
	/// A flexible handler for bool events in the form of a MonoBehaviour. Responses can be connected directly from the Unity Inspector.
	/// </summary>
	public class Vector3EventListener : MonoBehaviour
	{
		[SerializeField] private Vector3EventChannelSO _channel = default;

		public Vector3Event OnEventRaised;

		private void OnEnable()
		{
			if (_channel != null)
				_channel.OnEventRaised += Respond;
		}

		private void OnDisable()
		{
			if (_channel != null)
				_channel.OnEventRaised -= Respond;
		}

		private void Respond(Vector3 value)
		{
			if (OnEventRaised != null)
				OnEventRaised.Invoke(value);
		}
	}
}